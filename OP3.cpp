#include <Wire.h>
#include <Zumo32U4.h>

Zumo32U4LCD lcd;
Zumo32U4ButtonA buttonA;
Zumo32U4ButtonB buttonB;
Zumo32U4ButtonC buttonC;
Zumo32U4Buzzer buzzer;
Zumo32U4Motors motors;
Zumo32U4LineSensors lineSensors;
Zumo32U4ProximitySensors proxSensors;


//--------------------------zmienne i stałe globalne---------------------------------------------
static uint8_t sense8Left, sense8FrontLeft, sense8FrontRight, sense8Right;
static uint8_t only4times;
static uint8_t woddyJumpOnly5Times = 0;
static uint8_t difference;

static unsigned int lineSensorValues[3];

static uint16_t wait = 5000;
static uint16_t stateStartTime;
static uint16_t time;
static uint16_t timeLeft;

static bool wasThere = false;
static bool keyPressedA, keyPressedB, keyPressedC;
static bool stateWasChangedWhileAgo = true;
static bool backUP = true;


static const uint16_t border = 350;
static const uint16_t reverseStraight = 350;
static const uint16_t reverseSide = 270;
static const uint16_t reverseSideCrit = 200;
static const uint16_t REVERSE = 700;



enum where_not_2_go {
  MiddleLeft,
  MiddleRight,
  Left,
  Right,
  Front,
};

where_not_2_go sloggy;

enum Mode
{
  SELECT,
  RUSH,
  WOODPECKER,
  FLANK,
};

Mode mode = SELECT;

enum State
{
  Pausing,
  Backing,
  Waiting,
  Running,
};

State state = Pausing;
//---------------------------------SETUP--------------------------------------------------------

void setup()
{
  lineSensors.initThreeSensors();
  proxSensors.initThreeSensors();

  changeState(Pausing);
}

void loop()
{
  keyPressedA = buttonA.getSingleDebouncedPress();
  keyPressedB = buttonB.getSingleDebouncedPress();
  keyPressedC = buttonC.getSingleDebouncedPress();

  if (state == Pausing)
  {
    PausingSTATE();
  }
  else if (keyPressedA or keyPressedB or keyPressedC)
  {
    changeMode(SELECT);
    changeState(Pausing);
  }
  else if (state == Waiting)
  {
    WaitingSTATE();
  }
  else if (state == Backing)
  {
    BackingSTATE();
  }
  else
  {
    adjustToMODE();
  }
}


void backingUP() {
  switch(sloggy)
     {
       case MiddleLeft:
          motors.setSpeeds(400, -400);
          do{timeInThisState();} while(timeInThisState() < reverseSide);
            changeState(Running);
          break;

       case MiddleRight:
          motors.setSpeeds(-400, 400);
          do{timeInThisState();} while(timeInThisState() < reverseSide);
            changeState(Running);
          break;

       case Front:
          motors.setSpeeds(-400, 400);
          do{timeInThisState();} while(timeInThisState() < reverseStraight);
            changeState(Running);
          break;

       case Right:
          motors.setSpeeds(-400, 400);
          do{timeInThisState();} while(timeInThisState() < reverseSideCrit);
            changeState(Running);
          break;

       case Left:
          motors.setSpeeds(400, -400);
          do{timeInThisState();} while(timeInThisState() < reverseSideCrit);
            changeState(Running);
          break;
  }
}

void turnToEnemy() {

  refreshProx();

  if (OnLeft())
  {
    motors.setSpeeds(-310, 310);
    do
    {
      proxSensors.read();
      showProx();
    }
    while (proxSensors.countsFrontWithLeftLeds() < sense8Left);
  }
  else if (OnRight())
  {
    motors.setSpeeds(310, -310);
    do
    {
      proxSensors.read();
      showProx();
    }
    while (proxSensors.countsFrontWithRightLeds() < sense8Right);
  }

  do
  {
    proxSensors.read();
    showProx();
    difference = proxSensors.countsFrontWithRightLeds() - proxSensors.countsFrontWithLeftLeds();

    if (proxSensors.countsFrontWithRightLeds() + proxSensors.countsFrontWithLeftLeds() >= 4)
    {
      motors.setSpeeds(400, 400);
    }
    else if (difference > 1)
    {
      motors.setSpeeds(150, -150);
    }
    else if (difference < -1)
    {
      motors.setSpeeds(-150, 150);
    }

  }
  while (difference != 0);
}

bool amIOnBorder() {
   lineSensors.read(lineSensorValues);
   if (only4times == 4)
   {
     return true;
   }
   else
   {
     if (lineSensorValues[0] < border && lineSensorValues[1] < border && lineSensorValues[2] < border)
     {
      only4times++;
      nextMove(Front);
      amIOnBorder();
     }
     else if (lineSensorValues[0] < border && lineSensorValues[1] < border)
     {
      only4times++;
      nextMove(MiddleLeft);
      amIOnBorder();
     }
     else if (lineSensorValues[1] < border && lineSensorValues[2] < border)
     {
      only4times++;
      nextMove(MiddleRight);
      amIOnBorder();
     }
     else if (lineSensorValues[0] < border)
     {
      only4times++;
      nextMove(Left);
      amIOnBorder();
     }
     else if (lineSensorValues[2] < border)
     {
      only4times++;
      nextMove(Right);
      amIOnBorder();
     }
     else
     {
      return false;
     }
   }
}

uint16_t timeInThisState()
{
  return (uint16_t)(millis() - stateStartTime);
}

void changeState(uint8_t newState)
{
  state = (State)newState;
  stateWasChangedWhileAgo = true;
  stateStartTime = millis();
  lcd.clear();
}

void changeMode(uint8_t newMode)
{
  mode = (Mode)newMode;
  lcd.clear();
}

void nextMove(uint8_t nxt) {
  sloggy = (where_not_2_go)nxt;
}

void showProx() {
  lcd.gotoXY(0,1);
  lcd.print(proxSensors.countsLeftWithLeftLeds() );
  lcd.gotoXY(2,0);
  lcd.print(proxSensors.countsFrontWithLeftLeds() );
  lcd.gotoXY(5,0);
  lcd.print(proxSensors.countsFrontWithRightLeds() );
  lcd.gotoXY(7,1);
  lcd.print(proxSensors.countsRightWithRightLeds() );
}

void standStill() {
  motors.setSpeeds(0, 0);
}

void Rush() {
  motors.setSpeeds(320, 320);
}

void Ram () {
  motors.setSpeeds(400, 400);
}


void Woodpecker() {
  motors.setSpeeds(200, 200);
  if(woddyJumpOnly5Times == 4){
    changeMode(RUSH);
  } else {
    woddyJumpOnly5Times++;

    while(timeInThisState() < 200)
    {
      refreshProx();

      if(amIOnBorder())
         {
           motors.setSpeeds(-400, -400);
           backUP = true;
           changeState(Backing);
         }
      else if(OnFront() or OnLeft() or OnRight())
      {
        changeMode(RUSH);
        turnToEnemy();
        wasThere = true;
      }

    }
    if(wasThere != true){
      while(timeInThisState() < 3000){
        if(amIOnBorder())
          {
          changeState(Backing);
          backingUP();
          }

        refreshProx();

        if(OnFront() or OnLeft() or OnRight())
        {
          changeMode(RUSH);
          break;
        } else {
          motors.setSpeeds(0, 0);
        }
      }
    }
  }
}


void Flank() {
  motors.setSpeeds(400, -400);
  while(timeInThisState() < 100);
  motors.setSpeeds(290, 400);
  while(timeInThisState() < 1200){
    refreshProx();
    if(OnFront() or OnLeft()){
      wasThere = true;
      changeMode(RUSH);
      turnToEnemy();
      Ram();
    }
  }
  while(timeInThisState() < 1520){
    if(wasThere == true){
      changeMode(RUSH);
      turnToEnemy();
      Ram();
    } else {
      motors.setSpeeds(-400, 400);}
  }
}


bool OnFront() {
  if (sense8FrontRight > sense8Right && sense8FrontRight > sense8Left && sense8FrontLeft > sense8Right && sense8FrontLeft > sense8Left) {
    return true;
  } else {
    return false;
  }
}

bool OnLeft() {
  if (sense8Left >= sense8Right && sense8Left > sense8FrontRight && sense8Left > sense8FrontLeft) {
    return true;
  } else {
    return false;
  }
}

bool OnRight() {
  if (sense8Right >= sense8Left && sense8Right > sense8FrontRight && sense8Right > sense8FrontLeft) {
    return true;
  } else {
    return false;
  }
}

void refreshProx() {
  proxSensors.read();

  showProx();

  sense8Left = proxSensors.countsLeftWithLeftLeds();
  sense8FrontLeft = proxSensors.countsFrontWithLeftLeds();
  sense8FrontRight = proxSensors.countsFrontWithRightLeds();
  sense8Right = proxSensors.countsRightWithRightLeds();
}

void adjustToMODE() {
  switch(mode) {
    case RUSH:
        lcd.clear();
        only4times = 0;

         if(amIOnBorder())
         {
           motors.setSpeeds(-400, -400);
           backUP = true;
           changeState(Backing);
         }
         else
         {
            turnToEnemy();
            if(OnFront()){
              Ram();
            } else {
              Rush();
            }
         }
        break;


    case WOODPECKER:
        lcd.clear();
        only4times = 0;

        if(amIOnBorder())
        {
          motors.setSpeeds(-400, -400);
          backUP = true;
          changeState(Backing);
        }
        else
        {
          turnToEnemy();
          changeState(Running);
          Woodpecker();
        }
        break;


    case FLANK:
        lcd.clear();
        only4times = 0;

        if(amIOnBorder())
        {
          motors.setSpeeds(-400, -400);
          backUP = true;
          changeState(Backing);
          changeMode(RUSH);
        }
        else
        {
          turnToEnemy();
          changeState(Running);
          Flank();
          changeMode(RUSH);
        }
        break;

  }
}

void PausingSTATE() {
  motors.setSpeeds(0, 0);

  if (stateWasChangedWhileAgo)
  {
    stateWasChangedWhileAgo = false;
    lcd.gotoXY(1,0);
    lcd.print(F("Sloggy"));
    lcd.gotoXY(0,1);
    lcd.print(F("Ru Wo Fl"));
  }

  if (keyPressedA)
  {
    changeState(Waiting);
    changeMode(RUSH);
  }
  else if (keyPressedB)
  {
    changeState(Waiting);
    changeMode(WOODPECKER);
  }
  else if (keyPressedC)
  {
    changeState(Waiting);
    changeMode(FLANK);
  }
}

void WaitingSTATE() {
  motors.setSpeeds(0, 0);

  time = timeInThisState();

  if (time < wait)
  {
    timeLeft = wait - time;
    lcd.gotoXY(0, 0);
    lcd.print(timeLeft / 1000 % 10);
    lcd.print('.');
    lcd.print(timeLeft / 100 % 10);
  }
  else
  {
    changeState(Running);
  }
}

void BackingSTATE() {
  if (stateWasChangedWhileAgo)
  {
    stateWasChangedWhileAgo = false;
    lcd.print(F("BACKWARD"));
  }

  if (backUP == true && timeInThisState() > 200) {
    backUP = false;
    changeState(Backing);
    backingUP();
  }
}
