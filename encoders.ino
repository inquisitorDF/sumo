#include <Wire.h>
#include <Zumo32U4.h>

Zumo32U4LCD lcd;
Zumo32U4Encoders encoders;


uint16_t lastUpdateTime = millis() - 100;

void setup() {

}

void loop() {

    lcd.clear();
    lcd.gotoXY(1, 1);
    lcd.print(F("A \7B C"));

    uint16_t lastUpdateTime = millis() - 100;

    int16_t encCountsLeft = 0, encCountsRight = 0;
    char buf[4];

    while (true)
    {
        encCountsLeft += encoders.getCountsAndResetLeft();
        if (encCountsLeft < 0) { encCountsLeft += 1000; }
        if (encCountsLeft > 999) { encCountsLeft -= 1000; }

        encCountsRight += encoders.getCountsAndResetRight();
        if (encCountsRight < 0) { encCountsRight += 1000; }
        if (encCountsRight > 999) { encCountsRight -= 1000; }

      if ((uint16_t)(millis() - lastUpdateTime) > 50)
      {
        lastUpdateTime = millis();

        lcd.gotoXY(0, 0);

        sprintf(buf, "%03d", encCountsLeft);
        lcd.print(buf);
        lcd.gotoXY(5, 0);
        sprintf(buf, "%03d", encCountsRight);
        lcd.print(buf);
      }
    }

}
