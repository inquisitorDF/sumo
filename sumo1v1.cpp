#include <avr/pgmspace.h>
#include <Wire.h>
#include <Zumo32U4.h>   
#include <RobotIRremoteTools.h> //lib for IR

#define IRDA        5           //pin for IRDA (can be cahnged in IRremoteTools.cpp)
#define REMOTE_1    0xFF30CF    //code of button "1" on remote
#define REMOTE_2    0xFF18E7    //code of button "2" on remote

Zumo32U4LCD lcd;
Zumo32U4ButtonA buttonA;
Zumo32U4ButtonB buttonB;
Zumo32U4ButtonC buttonC;
Zumo32U4Buzzer buzzer;
Zumo32U4Motors motors;
Zumo32U4LineSensors lineSensors;
Zumo32U4ProximitySensors proxSensors;


//--------------------------zmienne i stałe globalne---------------------------------------------
uint8_t sense8Left, sense8FrontLeft, sense8FrontRight, sense8Right;
unsigned int lineSensorValues[3];
uint8_t only4times;
uint16_t wait = 5000;
uint16_t stateStartTime;
bool justChangedState = true;
bool backUP = true;
uint16_t timeNOW;
uint16_t time;
uint16_t timeLeft;



const uint16_t border = 350;
const uint16_t reverseStraight = 350;
const uint16_t reverseSide = 270;
const uint16_t reverseSideCrit = 200;
const uint16_t REVERSE = 700;

enum WaitingIsHard {
  One,
  Two
};

WaitingIsHard btn;

enum where_not_2_go {
  MiddleLeft,
  MiddleRight,
  Left,
  Right,
  Front,
};

where_not_2_go sloggy;


enum State
{
  Menu,
  Pausing,
  Backing,
  Waiting,
  Running,
};

State state = Menu;
//---------------------------------SETUP--------------------------------------------------------

void setup() 
{
  beginIRremote();

  lineSensors.initThreeSensors();
  proxSensors.initThreeSensors();

  changeState(Menu);
}

void loop() 
{
  bool keyPressedA = buttonA.getSingleDebouncedPress();
  bool keyPressedB = buttonB.getSingleDebouncedPress();
  bool keyPressedC = buttonC.getSingleDebouncedPress();

  if (state == Menu) 
  {
    motors.setSpeeds(0, 0);

    if (justChangedState) {
      justChangedState = false;
    }

    if (keyPressedA) {
      changeState(Waiting);
    }
    else if (keyPressedB) {
      changeBtn(One);
      changeState(Pausing);
    } 
    else if (keyPressedC) {
      changeBtn(Two);
      changeState(Pausing);
    }
  }
  else if (state == Pausing) {
    buzzer.playNote(NOTE_A(4), 200, 15);      //just for test
    if(IRrecived()) {
      switch(btn) {
        case One:
          buzzer.playNote(NOTE_A(4), 200, 15);      //just for test
          if (getIRresult() == REMOTE_1) {
            changeState(Waiting);
            }
            resumeIRremote();
          break;

          case Two:
          buzzer.playNote(NOTE_A(4), 200, 15);      //just for test
          if (getIRresult() == REMOTE_2) {
            changeState(Waiting);
            }
            resumeIRremote();
          break;
      }
    }
  }
  else if (state == Waiting) {
    motors.setSpeeds(0, 0);

    time = timeInThisState();

    if (time < wait) {
      timeLeft = wait - time;
    }
    else {
      changeState(Running);
    }
  }
  else if (state == Backing) {
    if (justChangedState)
    {
      justChangedState = false;
    }

    if (backUP == true && timeInThisState() > 200) {
      backUP = false;
      changeState(Backing);
      backingUP();
    }
  }
  else if (state == Running) {
    only4times = 0;

    if(amIOnBorder()) { 
      motors.setSpeeds(-400, -400);
      backUP = true;
      changeState(Backing);
    }
    else {
      turnToEnemy();
      Rush();
    }
  }
}


void backingUP() {
  switch(sloggy) 
     {
       case MiddleLeft:
          motors.setSpeeds(400, -400);
          do{timeInThisState();} while(timeInThisState() < reverseSide);
            changeState(Running);
          break;
       
       case MiddleRight:
          motors.setSpeeds(-400, 400);
          do{timeInThisState();} while(timeInThisState() < reverseSide);
            changeState(Running);
          break;

       case Front:
          motors.setSpeeds(-400, 400);
          do{timeInThisState();} while(timeInThisState() < reverseStraight);
            changeState(Running);
          break;

       case Right:
          motors.setSpeeds(-400, 400);
          do{timeInThisState();} while(timeInThisState() < reverseSideCrit);
            changeState(Running);
          break;

       case Left:
          motors.setSpeeds(400, -400);
          do{timeInThisState();} while(timeInThisState() < reverseSideCrit);
            changeState(Running);
          break;
  } 
}

void turnToEnemy() { 
  int error;

  proxSensors.read();

  sense8Left = proxSensors.countsLeftWithLeftLeds();
  sense8FrontLeft = proxSensors.countsFrontWithLeftLeds();
  sense8FrontRight = proxSensors.countsFrontWithRightLeds();
  sense8Right = proxSensors.countsRightWithRightLeds();

  if (OnLeft()) {
    motors.setSpeeds(-230, 230);

    do {
      proxSensors.read();
    } 
    while (proxSensors.countsFrontWithLeftLeds() < sense8Left);
  } 
  else if (OnRight()) {
    motors.setSpeeds(230, -230);

    do {
      proxSensors.read();
    } 
    while (proxSensors.countsFrontWithRightLeds() < sense8Right);
  }
  
  do {
    proxSensors.read();
    error = proxSensors.countsFrontWithRightLeds() - proxSensors.countsFrontWithLeftLeds();
    
    if(error > 0) {
      motors.setSpeeds(150, -150);
    } 
    else if(error < 0) {
      motors.setSpeeds(-150, 150);
    }
  } 
  while (error != 0);
}

bool amIOnBorder() {
   lineSensors.read(lineSensorValues);
   if (only4times == 1) 
   {
     return true;
   } 
   else 
   {
     if (lineSensorValues[0] < border && lineSensorValues[1] < border && lineSensorValues[2] < border) 
     {
      only4times++;
      nextMove(Front);
      amIOnBorder();
     } 
     else if (lineSensorValues[0] < border && lineSensorValues[1] < border) 
     {
      only4times++;
      nextMove(MiddleLeft);
      amIOnBorder();
     } 
     else if (lineSensorValues[1] < border && lineSensorValues[2] < border) 
     {
      only4times++;
      nextMove(MiddleRight);
      amIOnBorder();
     } 
     else if (lineSensorValues[0] < border) 
     {
      only4times++;
      nextMove(Left);
      amIOnBorder();
     } 
     else if (lineSensorValues[2] < border) 
     {
      only4times++;
      nextMove(Right);
      amIOnBorder();
     } 
     else 
     {
      return false;
     }
   }
}

void count() {
  motors.setSpeeds(0, 0);

    uint16_t time = timeInThisState();
    uint16_t timeLeft = wait - time;

    lcd.gotoXY(0, 0);
    lcd.print(timeLeft / 1000 % 10);
    lcd.print('.');
    lcd.print(timeLeft / 100 % 10);
}

uint16_t timeInThisState() {
  return (uint16_t)(millis() - stateStartTime);
}

void changeBtn(uint8_t newBtn) {
  btn = (WaitingIsHard)newBtn;
}

void changeState(uint8_t newState) {
  state = (State)newState;
  justChangedState = true;
  stateStartTime = millis();
  lcd.clear();
}

void nextMove(uint8_t nxt) {
  sloggy = (where_not_2_go)nxt;
}

void standStill() {
  motors.setSpeeds(0, 0);
}

void Rush() {
  motors.setSpeeds(400, 400);
}

bool OnFront() {
  if (sense8FrontRight > sense8Right && sense8FrontRight > sense8Left && sense8FrontLeft > sense8Right && sense8FrontLeft > sense8Left) {
    return true;
  } else {
    return false;
  }
}

bool OnLeft() {
  if (sense8Left >= sense8Right && sense8Left > sense8FrontRight && sense8Left > sense8FrontLeft) {
    return true;
  } else {
    return false;
  }
}

bool OnRight() {
  if (sense8Right >= sense8Left && sense8Right > sense8FrontRight && sense8Right > sense8FrontLeft) {
    return true;
  } else {
    return false;
  }
}